package Alien::NLopt;

# ABSTRACT: Build and Install the NLopt library

use v5.12;
use strict;
use warnings;

our $VERSION = 'v2.9.1.1';

use base qw( Alien::Base );

1;

# COPYRIGHT

__END__

=pod

=for stopwords
metacpan


=head1 SYNOPSIS

  use Alien::NLopt;

=head1 DESCRIPTION

This module finds or builds the I<NLopt> library.

=head1 USAGE

Please see L<Alien::Build::Manual::AlienUser> (or equivalently on L<metacpan|https://metacpan.org/pod/distribution/Alien-Build/lib/Alien/Build/Manual/AlienUser.pod>).

