package Alien::NLopt::Install::Files;

use v5.12;
use strict;
use warnings;

our $VERSION = '#[[ $dist->version ]]#';

require Alien::NLopt;

sub Inline { shift; Alien::NLopt->Inline( @_ ) }
1;

# COPYRIGHT

__END__

=for Pod::Coverage
Inline

=cut
